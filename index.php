<?php

/**
 * to test, how the composer works with library intervention/image
 * you need to install composer to test intervention/image
 *
 * 1. take image 'logo.png' from folder 'image', resize and save 
 * 2. take image 'background.jpg', resize, blur and insert logo.png to the center of the background and
 *	save it as result.jpg
 * @author Borys Suray <surayborys@gmail.com>
 */

// include composer autoload
require 'vendor/autoload.php';

// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;

// create an image manager instance with favored driver
$manager = new ImageManager(array('driver' => 'imagick'));

// to process images
$logo = $manager->make('images/logo.png')->resize(300, 200)->save();
$background = $manager->make('images/background.jpg')->resize(1024, 768)->blur(10)->insert($logo, 'center')->save('images/result.jpg');


